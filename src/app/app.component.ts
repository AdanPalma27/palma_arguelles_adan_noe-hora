import { Component } from '@angular/core';

@Component({
  selector: 'my-app',
  template: `<h1 class="title">HOLA {{name}}</h1> 
             <br> 
             <h1 class="title"></h1>
             <h1 id="hora" class="title">La hora es: {{tiempo}}</h1>
             <br>
             <h1 class="title">Navegador en uso: {{ver}}</h1>
             <br>
             <h1 class="title">Dispositivo en uso: {{mach}}</h1>
             <br>
             <h1 class="title">Sistema operativo en uso: {{so}}</h1>`,
   styles: [`
    .title {
      color: #241E1D;
      font-size: 40px;
    }`]
})

export class AppComponent{ 
  name = 'USUARIO'; 
  tiempo: Date;
  chrome: Boolean;
  fire: Boolean;
  exp: Boolean;
  ver: String;
  win: Boolean;
  linux: Boolean;
  mac: Boolean;
  so: String;
  disp: Boolean;
  mach: String;

  constructor(){

      this.chrome = (navigator.userAgent.indexOf("Chrome") != -1);
      if (this.chrome){
        this.ver = "Chrome";
        setInterval(() => this.tiempo = new Date(), 1000);
      }
      
      this.fire = (navigator.userAgent.indexOf("Firefox") != -1);
      if (this.fire){
        this.ver = "FireFox";
        setInterval(() => this.tiempo = new Date(), 1000);
      }
      
      this.exp = (navigator.userAgent.indexOf("Explorer") != -1);
      if (this.exp){
        this.ver = "Explorer";
        alert("Cámbiate a otro navegador anticuado!");
      }
      

      this.win = (window.navigator.appVersion.indexOf("Win") != -1);
      if(this.win){
        this.so = "Windows";
      }

      this.linux = (window.navigator.appVersion.indexOf("Linux") != -1);
      if(this.linux){
        this.so = "Linux";
      }

      this.mac = (window.navigator.appVersion.indexOf("Mac") != -1);
      if(this.mac){
        this.so = "Macintosh";
      }

      this.disp = (navigator.platform.indexOf("Win32") != -1);
      if(this.disp)
        this.mach = "Escritorio o Laptop";
      else
        this.mach = "Móvil";
    }


}